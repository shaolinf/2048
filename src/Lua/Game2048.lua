Lua2048 = {}

Lua2048.OK 		= true
Lua2048.FAILURE = false

--[[
 check if host_log function registered, or is not a function. If the host didn't register host_log function
 for lua script to invoke, redirect all log to 'print' function.
--]]
if ( host_log == nil or type(host_log) ~= "function" ) then
	-- assign a function to host_log
	host_log = print
end

--[[
 description: write log to host, can have more than one parameters, the 1st parameter is format
--]]
function Lua2048.log_tohost(...)
	-- format this log message and write to host
	host_log( string.format(...) )
end


