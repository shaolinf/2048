-- this is a sample script with all supported api invoked
-- look at each api's comment for how to use 
--
-- author : dali
-- contact: shaolinfu@gmail.com
-- 

-- global define the error codes
SUCCESS				= 0
KEY_IGNORED			= 1
GAME_ISOVER 		= 2

-- write log to the list of host, maximum log length is 4095
-- log will be truncated after index 4095
logToHost("SCRIPT_START")

-- set the interval between two steps, between 50 and 3000(ms)
-- the host will sleep a period after each move, 500 is the
-- default value for human in host.
logToHost("setStepInterval")
setStepInterval(500);
sleep(1000)

-- slide to left, right, up, down
-- possible returns: SUCCESS, KEY_IGNORED, GAME_ISOVER
logToHost("moveLeft")
moveLeft()

logToHost("moveUp")
moveUp()

logToHost("moveRight")
moveRight()

logToHost("moveDown")
moveDown()

-- test if able to slide to a direction
-- possible returns:
--   true  - able to slide
--   false - not able to slide
logToHost("testMoveLeft")
if ( testMoveLeft() == true ) then
	moveLeft()
end

logToHost("testMoveUp")
if ( testMoveUp() == true ) then
	moveUp()
end

logToHost("testMoveRight")
if ( testMoveRight() == true ) then
	moveRight()
end

logToHost("testMoveDown")
if ( testMoveDown() == true ) then
	moveDown()
end

-- get the cell values from host, the table index is from 0
cellValues = getCellTable();
-- loop through all table values and write to log
for i=0, #cellValues do
	logToHost( string.format("CELL_VALUES[%d] = %d", i, cellValues[i]) )
end
logToHost("waiting for 10 seconds...")
sleep(10000)

-- get single cell value by index
-- possible returns:
--   nil - index is out of range
valueAtIndexZero = getCellValue(0) 
if ( valueAtIndexZero ~= nil ) then
	logToHost( "valueAtIndexZero="..valueAtIndexZero )
end
sleep(1000)

valueAtIndexTwenty = getCellValue(20)
if ( valueAtIndexTwenty == nil ) then
	logToHost("INDEX_OUT_OF_RANGE")
end
sleep(1000)

-- get rows, columns
rows = getRows()
columns = getColumns()
logToHost( string.format("ROWS = %d, COLUMNS = %d", rows, columns) )

-- get empty cell count
emptyCellCount = getEmptyCellCount()
logToHost( "emptyCellCount="..emptyCellCount );

-- test if game is over
logToHost("isGameOver")
if ( not isGameOver() ) then
	logToHost("Game is not over.")
end
sleep(1000)
setStepInterval(200);

while ( not isGameOver() ) do
	logToHost("MOVE_LEFT trigged")
	if ( moveLeft() == KEY_IGNORED ) then
		logToHost("MOVE_LEFT ignored")
	end

	logToHost("MOVE_UP trigged")
	if ( moveUp() == KEY_IGNORED ) then
		logToHost("MOVE_UP ignored")
	end
	
	logToHost("MOVE_RIGHT trigged")
	if ( moveRight() == KEY_IGNORED ) then
		logToHost("MOVE_RIGHT ignored")
	end

	logToHost("MOVE_DOWN trigged")
	if ( moveDown() == KEY_IGNORED ) then
		logToHost("MOVE_DOWN ignored")
	end
end

if ( isGameOver() ) then
	logToHost("Game over.")
end

logToHost("SCRIPT_END")