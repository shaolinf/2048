
// 2048Dlg.cpp : implementation file
//

#include "stdafx.h"
#include "2048.h"
#include "2048Dlg.h"
#include "Profile.h"
#ifdef SCRIPT_SUPPORT
#include "Interpreter.h"
#endif
#include "Host.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CMy2048Dlg dialog

CMy2048Dlg::CMy2048Dlg(CWnd* pParent /*=NULL*/)
	: CDialog(CMy2048Dlg::IDD, pParent)
	, advancedViewShowing_( TRUE )
#ifdef SCRIPT_SUPPORT
	, m_strScriptFile(_T(""))
#endif
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CMy2048Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
#ifdef SCRIPT_SUPPORT
	DDX_Control(pDX, IDC_LIST_LOG, ListCtrlLog_);
	DDX_Control(pDX, IDC_EDIT_SCRIPT_FILE, m_editScriptFile);
	DDX_Text(pDX, IDC_EDIT_SCRIPT_FILE, m_strScriptFile);
	DDV_MaxChars(pDX, m_strScriptFile, 260);
#endif
}

BEGIN_MESSAGE_MAP(CMy2048Dlg, CDialog)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_BUTTON_RESTART, &CMy2048Dlg::OnBnClickedButtonRestart)
    ON_WM_CLOSE()
	ON_BN_CLICKED(IDC_BUTTON_UNDO, &CMy2048Dlg::OnBnClickedButtonUndo)
	ON_MESSAGE(WM_HISTORY_CHANGED, &CMy2048Dlg::OnHistoryChanged)
	ON_MESSAGE(WM_GAME_MOVE, &CMy2048Dlg::OnGameMove)
#ifdef SCRIPT_SUPPORT
	ON_BN_CLICKED(IDC_BUTTON_ADVANCED, &CMy2048Dlg::OnBnClickedButtonAdvanced)
	ON_BN_CLICKED(IDC_BUTTON_RUNSCRIPT, &CMy2048Dlg::OnBnClickedButtonRunscript)
	ON_MESSAGE( WM_LUASCRIPT, &CMy2048Dlg::OnLuaScript )
	ON_MESSAGE( WM_WRITELOG, &CMy2048Dlg::OnLogMessage )
#endif
	ON_WM_TIMER()
	ON_WM_ERASEBKGND()
END_MESSAGE_MAP()


// CMy2048Dlg message handlers

BOOL CMy2048Dlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	//SetIcon(m_hIcon, TRUE);			// Set big icon
	//SetIcon(m_hIcon, FALSE);		// Set small icon

	

	//////////////////////////////////////////////////////////////////////////
#ifdef SCRIPT_SUPPORT
	// initialize the log message list
	CRect rect;
	ListCtrlLog_.GetClientRect(&rect);

	// add two columns on the report view, 'Time' and 'Messages'
	ListCtrlLog_.InsertColumn(0, _T("Time"), LVCFMT_LEFT, 160);
	ListCtrlLog_.InsertColumn(1, _T("Messages"), LVCFMT_LEFT, rect.Width() - 160);


	// allow full row select
	ListCtrlLog_.SetExtendedStyle( LVS_EX_FULLROWSELECT ); 

	/////////////////////////////////////////////////////////////////////////

    GetDlgItem(IDC_EDIT_SCRIPT_FILE)->SetWindowText(_T(".\\readme.lua"));
#endif

	Game::initialize();
	

    CRect windowRect;
    CRect advBtnRect;

    GetWindowRect( &windowRect );
    advancedViewHeight_ = windowRect.Height();
    windowWidth_ = windowRect.Width();

    ///////////////////////////////////////////////////////////////////////////
#ifdef SCRIPT_SUPPORT
    GetDlgItem(IDC_BUTTON_ADVANCED)->ShowWindow( SW_SHOW );
#endif
	///////////////////////////////////////////////////////////////////////////

    // Advanced button bottom position
    GetDlgItem(IDC_BUTTON_ADVANCED)->GetWindowRect( &advBtnRect );
    basicViewHeight_ = advBtnRect.bottom - windowRect.top + 15;

    switchView();
	
    CRect clientRect;
    GetClientRect( &clientRect );
    
    int margin = (clientRect.Height() - CANVAS_WIDTH)/ 2;
    margin = (margin > 15) ? 15 : margin;

	canvasLocation_.x = margin;
	canvasLocation_.y = margin;

	canvas_.initialize( windowWidth_ - margin * 2, CANVAS_WIDTH );

	
	Profile::getInstance()->loadLastContextTo( *Game::getInstance() );

	return TRUE;  // return TRUE  unless you set the focus to a control
}

BOOL CMy2048Dlg::PreTranslateMessage(MSG* pMsg)
{
	static bool keyIsDown = false;

	if ( pMsg->message == WM_KEYDOWN )
	{
		if ( !keyIsDown )
		{
			switch( pMsg->wParam )
			{
			case VK_LEFT:
				
				PostMessage( WM_GAME_MOVE, LEFT );
				break;

			case VK_RIGHT:
				PostMessage( WM_GAME_MOVE, RIGHT );
				break;

			case VK_UP:
				PostMessage( WM_GAME_MOVE, UP );
				break;

			case VK_DOWN:
				PostMessage( WM_GAME_MOVE, DOWN );
				break;

			default:
				break;
			}
			keyIsDown = true;
		}
	}
	else if ( pMsg->message == WM_KEYUP )
	{
		keyIsDown = false;
	}
	return FALSE;
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CMy2048Dlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CPaintDC dc(this);

		canvas_.draw( &dc, canvasLocation_.x, canvasLocation_.y );		
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CMy2048Dlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CMy2048Dlg::OnBnClickedButtonRestart()
{
	Game::restart();

	Invalidate();
}

void CMy2048Dlg::OnClose()
{
	Profile::getInstance()->saveContext( *Game::getInstance() );

    CDialog::OnClose();
}

void CMy2048Dlg::OnBnClickedButtonUndo()
{
	Game::reverseOneStep();

	Invalidate();
}

LRESULT CMy2048Dlg::OnHistoryChanged(WPARAM wParam, LPARAM lParam)
{
	UINT historyCount = Game::getHistoryCount();

	TCHAR text[32];

	_stprintf_s( text, _T("UNDO(%d)"), historyCount );

	GetDlgItem(IDC_BUTTON_UNDO)->SetWindowText(text);
	GetDlgItem(IDC_BUTTON_UNDO)->EnableWindow( historyCount > 0 ? TRUE : FALSE);
	
	if ( historyCount <= 0 )
	{
		AfxGetMainWnd()->SetFocus();
	}

	return 0;
}

LRESULT CMy2048Dlg::OnGameMove(WPARAM wParam, LPARAM lParam)
{
	Direction direction = static_cast<Direction>(wParam);

	Game::onKey( direction );

	Invalidate();

	return 0;
}

void CMy2048Dlg::switchView()
{
    int height = advancedViewShowing_ ? basicViewHeight_ : advancedViewHeight_;

    SetWindowPos(NULL, 0, 0, windowWidth_, height,
        SWP_NOMOVE | SWP_NOACTIVATE | SWP_NOZORDER);

    advancedViewShowing_ = !advancedViewShowing_;
}

#ifdef SCRIPT_SUPPORT

void CMy2048Dlg::OnBnClickedButtonAdvanced()
{
    switchView();
}

UINT CMy2048Dlg::LuaScriptExecThreadProc(LPVOID lParam)
{
	CMy2048Dlg* pHostDlg = static_cast<CMy2048Dlg*>( lParam );

	// tell the GUI thread the thread already started
	pHostDlg->PostMessage( WM_LUASCRIPT, LUA_BEGIN );

	// create a lua interpreter
	Interpreter interpreter;

	CString strMsg;
	CString* pMsg = NULL;

	// run the lua script file
	if ( !interpreter.RunLuaFile( pHostDlg->m_strScriptFile, strMsg ) )
	{
		pMsg = new CString( strMsg );
	}
	
	// message handler need to free the pMsg object
	pHostDlg->PostMessage( WM_LUASCRIPT, LUA_END, (LPARAM)pMsg );
	
	return 0;
}

void CMy2048Dlg::OnBnClickedButtonRunscript()
{
	UpdateData(TRUE);

	AfxBeginThread( CMy2048Dlg::LuaScriptExecThreadProc, this );
}

LRESULT CMy2048Dlg::OnLuaScript(WPARAM wParam, LPARAM lParam)
{
	// LUA execution begin
	if ( wParam == LUA_BEGIN )
	{
		// disable the GUI buttons
		GetDlgItem(IDC_BUTTON_RUNSCRIPT)->EnableWindow( FALSE );

		ListCtrlLog_.DeleteAllItems();
	}
	// LUA execution end
	else if ( wParam == LUA_END )
	{
		// write message to log list if we have a message
		if ( lParam != NULL )
		{
			CString* pMsg = (CString*)lParam;
			Host::getInstance()->WriteLog( *pMsg );
			delete pMsg;
		}
		else
		{
			// Host::getInstance()->WriteLog(_T("Script is finished."));
		}

		// enable the GUI buttons
		GetDlgItem(IDC_BUTTON_RUNSCRIPT)->EnableWindow( TRUE );
	}

	return 0;
}

LRESULT CMy2048Dlg::OnLogMessage(WPARAM wParam, LPARAM lParam)
{
	CString* pLogMsg = (CString*)wParam;

	LVITEM lvi;
	CString strItem;
	CTime t = CTime::GetCurrentTime();

	// formate the current date time: MM/DD/YY HH:mm:SS
	strItem.Format(_T("%02d/%02d/%02d %02d:%02d:%02d"), t.GetMonth(), t.GetDay(), t.GetYear(),
		t.GetHour(),  t.GetMinute(), t.GetSecond());

	// insert a new item
	lvi.mask = LVIF_TEXT;
	lvi.iItem = ListCtrlLog_.GetItemCount();
	lvi.iSubItem = 0;
	lvi.pszText = (LPTSTR)(LPCTSTR)strItem;
	ListCtrlLog_.InsertItem(&lvi);

	// set message of the item
	lvi.iSubItem = 1;
	lvi.pszText = (LPTSTR)(LPCTSTR)(*pLogMsg);
	ListCtrlLog_.SetItem(&lvi);

	// scroll to bottom of the list
	ListCtrlLog_.EnsureVisible(lvi.iItem, FALSE);

	// free the message object
	delete pLogMsg;

	return 0;
}
#endif

void CMy2048Dlg::OnTimer(UINT_PTR nIDEvent)
{
	if ( nIDEvent == ANIMATION_TIMER_ID )
	{
		Invalidate( );
	}

	CDialog::OnTimer(nIDEvent);
}

BOOL CMy2048Dlg::OnEraseBkgnd(CDC* pDC)
{
	return FALSE;
}
