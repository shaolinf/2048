
// 2048Dlg.h : header file
//

#pragma once

#include "Game.h"
#include "afxcmn.h"
#include "afxwin.h"
#include "Canvas.h"

// CMy2048Dlg dialog
class CMy2048Dlg : public CDialog
{
// Construction
public:
	CMy2048Dlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_MY2048_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	virtual BOOL PreTranslateMessage(MSG* pMsg);


// Implementation
protected:
	HICON m_hIcon;

	Canvas	canvas_;
	POINT	canvasLocation_;

    UINT    basicViewHeight_;
    UINT    advancedViewHeight_;
    UINT    windowWidth_;

	
    BOOL    advancedViewShowing_;

    void    switchView();

#ifdef SCRIPT_SUPPORT
	static UINT LuaScriptExecThreadProc( LPVOID lParam );

    LRESULT OnLuaScript(WPARAM wParam, LPARAM lParam);
	LRESULT OnLogMessage(WPARAM wParam, LPARAM lParam);
#endif


	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg LRESULT OnHistoryChanged(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnGameMove(WPARAM wParam, LPARAM lParam);

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButtonRestart();
    afx_msg void OnClose();
	afx_msg void OnBnClickedButtonUndo();

#ifdef SCRIPT_SUPPORT
	afx_msg void OnBnClickedButtonAdvanced();
	afx_msg void OnBnClickedButtonRunscript();

    CListCtrl ListCtrlLog_;
	CEdit m_editScriptFile;
	CString m_strScriptFile;
#endif
	
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
};
