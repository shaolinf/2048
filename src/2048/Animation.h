#pragma once
#include "GameDefs.h"

struct AnimationAction
{
	Direction	direction;
	WORD		pixelsOffset;
	WORD		pixelsEaten;

	enum Status
	{
		ANIMATION_END,
		ANIMATION_CONTINUE
	};

	AnimationAction::Status stepForward();

	void getOffset( int& x, int& y );
};
