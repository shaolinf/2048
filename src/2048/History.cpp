#include "StdAfx.h"
#include "History.h"

History::History( UINT limit )
	: Limit_( limit )
{
}

History::~History(void)
{
}

void	History::append( const Context& context )
{
	if ( getCount()  >= Limit_ )
	{
		HistoryContext_.pop_front();
	}

	HistoryContext_.push_back( context );
	notifyChange_();
}
	
void	History::clear(void)
{
	HistoryContext_.clear();
	notifyChange_();
}

UINT	History::getCount(void) const
{
	return HistoryContext_.size();
}

void	History::reverseTo( Context& context )
{
	if ( getCount() > 0 )
	{
		context = HistoryContext_.back();
		HistoryContext_.pop_back();
		notifyChange_();
	}
}

void	History::notifyChange_()
{
	AfxGetMainWnd()->PostMessage(WM_HISTORY_CHANGED);
}