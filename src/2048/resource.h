//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by 2048.rc
//
#define IDD_MY2048_DIALOG               102
#define IDR_MAINFRAME                   128
#define IDC_BUTTON1                     1000
#define IDC_BUTTON_RESTART              1000
#define IDC_BUTTON2                     1001
#define IDC_BUTTON_REVERSE              1001
#define IDC_BUTTON_UNDO                 1001
#define IDC_BUTTON_ADVANCED             1002
#define IDC_LIST_LOG                    1003
#define IDC_EDIT1                       1004
#define IDC_EDIT_SCRIPT_FILE            1004
#define IDC_BUTTON_RUNSCRIPT            1005

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1005
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
