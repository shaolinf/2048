#include "StdAfx.h"
#include "CellColors.h"

CellColors::CellColors(void)
{
	colorMap_[ 2 ] = RGB(245,222,179);	
	colorMap_[ 4 ] = RGB(221,160,221);	
	colorMap_[ 8 ] = RGB(144,238,144);		
	colorMap_[ 16 ] = RGB(189,183,107);	
	colorMap_[ 32 ] = RGB(255,140,0);	
	colorMap_[ 64 ] = RGB(30,144,255);	
	colorMap_[ 128 ] = RGB(255,248,220);
	colorMap_[ 256 ] = RGB(255,248,220);
	colorMap_[ 512 ] = RGB(255,248,220);
	colorMap_[ 1024 ] = RGB(255,248,220);
	colorMap_[ 2048 ] = RGB(255,248,220);
}

CellColors::~CellColors(void)
{
}

CellColors* CellColors::instance_	=	NULL;

CellColors* CellColors::getInstance()
{
	if ( instance_ == NULL )
		instance_ = new CellColors();

	return instance_;
}

COLORREF	CellColors::get( UINT val )
{
	if ( colorMap_.find( val ) != colorMap_.end() )
	{
		return colorMap_[ val ];
	}

	return DEFAULT_CELL_COLOR;
}