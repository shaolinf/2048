#include "StdAfx.h"
#include "Utility.h"

int randEx()
{
	LARGE_INTEGER seed;
	QueryPerformanceFrequency(&seed);
	QueryPerformanceCounter(&seed);
	srand(static_cast<UINT>(seed.QuadPart));

	return rand();
}