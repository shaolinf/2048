#pragma once

class Canvas
{
public:
	Canvas(void);
	~Canvas(void);

	void	initialize( int width, int height );

	void	draw( CDC* pDC, int x, int y );

private:

	void	drawMain_();
	void	drawInfoBkgnd_();
	void	drawInfo_();

	CRect	scoreDisplayRect_;
	CRect	bestScoreDisplayRect_;
	CRect	gameOverDisplayRect_;
	CRect	infoAreaRect_;

	CFont	scoreFont_;

	CDC		memDC_;
	CBitmap memBmp_;

	int		width_;
	int		height_;
};
