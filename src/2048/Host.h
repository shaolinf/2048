#pragma once

class Host
{
public:
	static Host*	getInstance();

	void WriteLog( const TCHAR* pLogMsg );

    void RefreshGUI(void);

	void EnableAnimationTimer( BOOL bEnable );

private:
	static	Host*	instance_;

	Host(void);
	~Host(void);
};
