#include "StdAfx.h"
#include "Host.h"
#include "GameDefs.h"

Host* Host::instance_ = NULL;

Host::Host(void)
{
}

Host::~Host(void)
{
}

Host* Host::getInstance()
{
	if ( NULL == instance_ )
	{
		instance_ = new Host();
	}

	return instance_;
}

void Host::WriteLog( const TCHAR* pLogMsg )
{
	CWnd* pHostDlg = AfxGetApp()->m_pMainWnd;

	// make a copy of the message, the message handler of WM_WRITELOG
	// need to free this CString object
	CString* pLogCStr = new CString(pLogMsg);

	// write log will be called from worker thread, so we PostMessage to GUI thread
	pHostDlg->PostMessage( WM_WRITELOG, (WPARAM)pLogCStr );
}

void Host::RefreshGUI(void)
{
    CWnd* pHostDlg = AfxGetApp()->m_pMainWnd;

	pHostDlg->Invalidate();
}

void Host::EnableAnimationTimer( BOOL bEnable )
{
	CWnd* pHostDlg = AfxGetApp()->m_pMainWnd;
	
	if ( bEnable )
	{
		pHostDlg->SetTimer( ANIMATION_TIMER_ID, ANIMATION_TIMER_ELAPSE, NULL );
	}
	else
	{
		pHostDlg->KillTimer( ANIMATION_TIMER_ID );
	}
}