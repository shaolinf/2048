#include "StdAfx.h"
#include "Animation.h"

AnimationAction::Status AnimationAction::stepForward()
{
	pixelsEaten *= 3;

	if ( pixelsEaten >= pixelsOffset )
	{
		pixelsEaten  = pixelsOffset;

		return ANIMATION_END;
	}

	return ANIMATION_CONTINUE;
}

void AnimationAction::getOffset( int& x, int& y )
{
	x = y = 0;

	switch ( direction )
	{
	case LEFT:
		x = -pixelsEaten;
		break;
	case RIGHT:
		x = pixelsEaten;
		break;
	case UP:
		y = -pixelsEaten;
		break;
	case DOWN:
		y = pixelsEaten;
		break;
	default:
		break;
	}
}