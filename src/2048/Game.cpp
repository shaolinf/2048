#include "StdAfx.h"
#include <stdlib.h>

#include "Game.h"
#include "CellColors.h"
#include "Host.h"
#include "Profile.h"

Game::Locker::Locker()
{
	Game::cs_.Lock();
}

Game::Locker::~Locker()
{
	Game::cs_.Unlock();
}

Game* Game::instance_ = NULL;
CCriticalSection Game::cs_;

Game* Game::getInstance()
{
	if( NULL == instance_ )
	{
		instance_ = new Game();
	}

	return instance_;
}

void	Game::draw( CDC* pDC )
{
	Locker locker;

	getInstance()->draw_( pDC );
}

bool	Game::isOver()
{
	Locker locker;

	return getInstance()->isOver_();
}

void	Game::initialize( void )
{
	Locker locker;

	getInstance()->initialize_();
}

GameError	Game::getCellValue(int index, UINT& value) 
{
	if ( index >= 0 && index < CANVAS_CELL_COUNT )
	{
		Locker locker;

		value = getInstance()->getCellValue_( index );
		return SUCCESS;
	}

	return INDEX_OUTOF_RANGE;
}

UINT	Game::getHistoryCount( void )
{
	Locker locker;

	return getInstance()->getHistoryCount_();
}

UINT	Game::getScore()
{
	Locker locker;

	return getInstance()->getScore_();
}

UINT	Game::getBestScore()
{
	Locker locker;

	return Profile::getInstance()->getBestScore();
}

GameError	Game::onKey( Direction direction )
{
	Locker locker;

	return getInstance()->onKey_( direction );
}

void	Game::restart()
{
	Locker locker;

	getInstance()->restart_();
}

void	Game::reverseOneStep( void )
{
	Locker locker;

	getInstance()->reverseOneStep_();
}

bool Game::testMove( Direction direction )
{
	Locker locker;

	return getInstance()->testMove_( direction );
}


Game::Game(void)
: History_(HISTORY_LIMIT_COUNT)
, NewScore_(0)
, InAnimation_(FALSE)
, AnimationEnabled_(ANIMATION_ENABLED)
{
	TextFont_.CreateFont(
		32,                        // nHeight
		0,                         // nWidth
		0,                         // nEscapement
		0,                         // nOrientation
		FW_NORMAL,                 // nWeight
		FALSE,                     // bItalic
		FALSE,                     // bUnderline
		0,                         // cStrikeOut
		ANSI_CHARSET,              // nCharSet
		OUT_DEFAULT_PRECIS,        // nOutPrecision
		CLIP_DEFAULT_PRECIS,       // nClipPrecision
		DEFAULT_QUALITY,           // nQuality
		DEFAULT_PITCH | FF_SWISS,  // nPitchAndFamily
		_T("Arial") );             // lpszFacename 
}

Game::~Game(void)
{
	TextFont_.DeleteObject(); 
}

void	Game::draw_( CDC* pDC )
{
	drawBackground_( pDC );

	drawCells_( pDC );
}

bool	Game::isOver_() const
{
	return IsOver_;
}

UINT	Game::getScore_( ) const
{
	return Score_;
}

GameError	Game::onKey_( Direction direction )
{
	if ( IsOver_ )
	{
		return GAME_ISOVER;
	}

	// Reset pending context
	resetPendingVal_();
	resetPendingAnimationActions_();

	switch ( direction )
	{
	case LEFT:
		onMoveLeft_();
		break;

	case RIGHT:
		onMoveRight_();
		break;

	case UP:
		onMoveUp_();
		break;

	case DOWN:
		onMoveDown_();
		break;

	default:
		break;
		
	}

	BOOL haveChanges = isContextChanged_();
	
	// TODO:: add lua option to enable this log
	// writeDirectionLog_( direction, haveChanges );

	if ( !haveChanges )
	{
		return KEY_IGNORED;
	}

	// Only phase in the pending context if there's some changes
	phaseInPendingVal_();

    return SUCCESS;
}

void	Game::restart_()
{
	initialize_();
}

bool	Game::detectGameOver_()
{
	for ( int i = 0; i < CANVAS_CELL_COUNT; ++ i )
	{
		// still have empty cells
		if (CurrentVal_[i] == EMPTY_CELL_VALUE)
		{
			return false;
		}
	}

	bool gameOver = true;

	for ( int row = 0; row < getRows_(); ++ row )
	{
		for ( int col = 0; col < getColumns_(); ++ col )
		{
			int  cellIndex = getCellIndex_(row, col);
			
			UINT val = CurrentVal_[cellIndex];
			
			// up
			cellIndex = getCellIndex_(row -1, col);
			if ( cellIndex != INVALID_CELL_INDEX && CurrentVal_[cellIndex] == val)
			{
				gameOver = false;
			}

			// down
			cellIndex = getCellIndex_(row +1, col);
			if ( cellIndex != INVALID_CELL_INDEX && CurrentVal_[cellIndex] == val)
			{
				gameOver = false;
			}

			// left
			cellIndex = getCellIndex_(row, col -1);
			if ( cellIndex != INVALID_CELL_INDEX && CurrentVal_[cellIndex] == val)
			{
				gameOver = false;
			}

			// right
			cellIndex = getCellIndex_(row, col +1);
			if ( cellIndex != INVALID_CELL_INDEX && CurrentVal_[cellIndex] == val)
			{
				gameOver = false;
			}
		}
	}

	return gameOver;
}

void	Game::drawBackground_( CDC* pDC )
{
	CBrush bkgndBrush( BKGND_COLOR );
	CRect rcBkgnd( 0, 0, CANVAS_WIDTH, CANVAS_WIDTH );
	pDC->FillRect( &rcBkgnd, &bkgndBrush );

	CBrush  emptyCellBrush( EMPTY_CELL_COLOR );
	CPen	emptyCellBorderPen( PS_SOLID, 1, EMPTY_CELL_COLOR );

	for ( int i = 0; i < COUNT_OF(CurrentVal_); ++ i )
	{
		int row = i / CANVAS_COLUMNS;
		int col = i % CANVAS_COLUMNS;

		int left = CANVAS_SPACE  + (CANVAS_SPACE + CANVAS_CELL_WIDTH) * col;
		int top  = CANVAS_SPACE  + (CANVAS_SPACE + CANVAS_CELL_WIDTH) * row;

		CRect cellRect( left, top, left + CANVAS_CELL_WIDTH, top + CANVAS_CELL_WIDTH );
		
		CBrush* pOldBrush = pDC->SelectObject( &emptyCellBrush );
		CPen*	pOldPen	  = pDC->SelectObject( &emptyCellBorderPen );
		pDC->RoundRect( &cellRect, ROUND_CELL_POINT );
		pDC->SelectObject( pOldBrush );
		pDC->SelectObject( pOldPen );
	}
}

void	Game::drawCells_( CDC* pDC )
{
	BOOL inAnimation = FALSE;

	if ( AnimationEnabled_ && !InAnimation_ )
	{
		Host::getInstance()->EnableAnimationTimer( FALSE );
	}

	for ( int i = 0; i < CANVAS_CELL_COUNT; ++ i )
	{
		int row = i / CANVAS_COLUMNS;
		int col = i % CANVAS_COLUMNS;

		int left = CANVAS_SPACE  + (CANVAS_SPACE + CANVAS_CELL_WIDTH) * col;
		int top  = CANVAS_SPACE  + (CANVAS_SPACE + CANVAS_CELL_WIDTH) * row;
		CRect cellRect( left, top, left + CANVAS_CELL_WIDTH, top + CANVAS_CELL_WIDTH );

		int cellValueToDraw = EMPTY_CELL_VALUE;
		if ( AnimationEnabled_ && InAnimation_ )
		{
			cellValueToDraw = PriviousVal_[i];

			if ( CurrentAnimationActions_[i].direction != NONE )
			{
				int offsetX = 0;
				int offsetY = 0;

				CurrentAnimationActions_[i].getOffset( offsetX, offsetY );

				cellRect.OffsetRect( offsetX, offsetY );

				AnimationAction::Status status = CurrentAnimationActions_[i].stepForward();
				if ( status == AnimationAction::ANIMATION_CONTINUE )
				{
					inAnimation = TRUE;
				}
			}
		}
		else
		{
			cellValueToDraw = CurrentVal_[i];
		}

		if ( cellValueToDraw != EMPTY_CELL_VALUE )
		{
			COLORREF cellColor = CellColors::getInstance()->get( cellValueToDraw );
			CBrush cellBrush( cellColor );
			CPen   cellBorderPen(  PS_SOLID, 1, cellColor );

			CBrush* pOldBrush = pDC->SelectObject( &cellBrush );
			CPen*   pOldPen   = pDC->SelectObject( &cellBorderPen );

			pDC->RoundRect( &cellRect, ROUND_CELL_POINT );

			TCHAR text[16];
			_stprintf_s( text, _T("%d"), cellValueToDraw );
			
			CFont* pOldFont = pDC->SelectObject( &TextFont_ );

			int bkMode = pDC->SetBkMode( TRANSPARENT );
			pDC->DrawText( text, _tcslen(text), &cellRect, DT_CENTER|DT_VCENTER|DT_SINGLELINE );
			pDC->SetBkMode( bkMode );

			// restore context
			pDC->SelectObject( pOldFont );
			pDC->SelectObject( pOldPen );
			pDC->SelectObject( pOldBrush );	
		}
	}

	if ( AnimationEnabled_ )
	{
		InAnimation_ = inAnimation;
	}
}

UINT	Game::getCellValue_( int row, int col ) const
{
	int cellIndex = getCellIndex_( row, col );
	ASSERT( cellIndex >= 0 && cellIndex < CANVAS_CELL_COUNT );

	return CurrentVal_[cellIndex];
}

UINT	Game::getCellValue_( int index ) const
{
	ASSERT( index >= 0 && index < CANVAS_CELL_COUNT );

	return CurrentVal_[index];
}

int		Game::getCellIndex_( int row, int col ) const
{
	if ( row < 0 || row >= getRows_() 
		|| col < 0 || col >= getColumns_() )
	{
		return INVALID_CELL_INDEX;
	}

	return row * CANVAS_COLUMNS + col;
}

int		Game::getRows_( void ) const
{
	return CANVAS_ROWS;
}

int		Game::getColumns_( void ) const
{
	return CANVAS_COLUMNS;
}

UINT	Game::getHistoryCount_( void ) const
{
	return History_.getCount();
}

void	Game::initialize_( void )
{
	resetCurrentVal_();
	resetPendingVal_();
	resetCurrentAnimationActions_();
	resetPendingAnimationActions_();

	for ( int times = 0; times < INITIAL_CELLS_COUNT; ++ times )
	{
		int index = lookupEmptyCell_( );
		if ( index != INVALID_CELL_INDEX )
		{
			CurrentVal_[ index ] = MINIMUM_CELL_VALUE;
		}
	}

	Score_ = 0;
	IsOver_ = false;
	History_.clear();
}

BOOL	Game::isContextChanged_( void )
{
	int result = memcmp( CurrentVal_, PendingVal_, sizeof(PendingVal_) );

    return (result == 0) ? FALSE : TRUE;
}

int		Game::lookupEmptyCell_( void ) const
{
	vector<int> emptyCells;

	for ( int i = 0; i < COUNT_OF(CurrentVal_); ++ i )
	{
		// free cell
		if ( CurrentVal_[i] == EMPTY_CELL_VALUE )
		{
			emptyCells.push_back( i );
		}
	}

	// still have free cell
	if ( emptyCells.size() > EMPTY_CELL_VALUE )
	{
		srand ( (UINT)time(NULL) );
		int randNum = randEx() % emptyCells.size();

		return emptyCells[randNum];
	}
	else
	{
		return INVALID_CELL_INDEX;
	}
}

void	Game::onMoveLeft_()
{
	for ( int row = 0; row < getRows_(); ++ row )
	{
		UINT lastCellVal = INVALID_CELL_VALUE;
		int newCol = -1;

		for ( int col = 0; col < getColumns_(); ++ col )
		{
			int curCellVal = getCellValue_(row, col);
			if ( curCellVal != EMPTY_CELL_VALUE )
			{
				if( curCellVal != lastCellVal )
				{
					lastCellVal = curCellVal;
					newCol ++;

					setCellValue_( PendingVal_, row, newCol, curCellVal );
				}
				else
				{
					setCellValue_( PendingVal_, row, newCol, lastCellVal * 2 );
					
					NewScore_ += lastCellVal * 2;

					// reset last value
					lastCellVal = INVALID_CELL_VALUE;
				}

				setPendingAnimationAction_( row, col, LEFT, col - newCol );
			}
		}
	}
}

void	Game::onMoveRight_()
{
	for ( int row = 0; row < getRows_(); ++ row )
	{
		UINT lastCellVal = INVALID_CELL_VALUE;
		int newCol = getColumns_();

		for ( int col = getColumns_() - 1; col >=0; --col )
		{
			int curCellVal = getCellValue_(row, col);
			if ( curCellVal != EMPTY_CELL_VALUE )
			{
				if( curCellVal != lastCellVal )
				{
					lastCellVal = curCellVal;
					newCol --;
					setCellValue_( PendingVal_, row, newCol, curCellVal );
				}
				else
				{
					setCellValue_( PendingVal_, row, newCol, lastCellVal * 2 );
					
					NewScore_ += lastCellVal * 2;

					// reset last value
					lastCellVal = INVALID_CELL_VALUE;
				}

				setPendingAnimationAction_( row, col, RIGHT, newCol - col);
			}
		}
	}
}

void	Game::onMoveUp_()
{
	for ( int col = 0; col < getColumns_(); ++ col )
	{
		UINT lastCellVal = INVALID_CELL_VALUE;
		int newRow = -1;

		for ( int row = 0; row < getRows_(); ++ row )
		{
			int curCellVal = getCellValue_(row, col);
			if ( curCellVal != EMPTY_CELL_VALUE )
			{
				if( curCellVal != lastCellVal )
				{
					lastCellVal = curCellVal;
					newRow ++;
					setCellValue_( PendingVal_, newRow, col, curCellVal );
				}
				else
				{
					setCellValue_( PendingVal_, newRow, col, lastCellVal * 2 );
					
					NewScore_ += lastCellVal * 2;

					// reset last value
					lastCellVal = INVALID_CELL_VALUE;
				}

				setPendingAnimationAction_( row, col, UP, row - newRow );
			}
		}
	}
}

void	Game::onMoveDown_()
{
	for ( int col = 0; col < getRows_(); ++ col )
	{
		UINT lastCellVal = INVALID_CELL_VALUE;
		int newRow = getRows_();

		for ( int row = getRows_() - 1; row >=0; --row )
		{
			int curCellVal = getCellValue_(row, col);
			if ( curCellVal != EMPTY_CELL_VALUE )
			{
				if( curCellVal != lastCellVal )
				{
					lastCellVal = curCellVal;
					newRow --;
					setCellValue_( PendingVal_, newRow, col, curCellVal );
				}
				else
				{
					setCellValue_( PendingVal_, newRow, col, lastCellVal * 2 );
					
					NewScore_ += lastCellVal * 2;

					// reset last value
					lastCellVal = INVALID_CELL_VALUE;
				}

				setPendingAnimationAction_( row, col, DOWN, newRow - row );
			}
		}
	}
}

void	Game::updateScore_( void )
{
	Score_ += NewScore_;

	if ( Score_ > Profile::getInstance()->getBestScore() )
	{
		Profile::getInstance()->updateBestScore( Score_ );
	}

	NewScore_ = 0;
}

void	Game::reverseOneStep_( void )
{
	History_.reverseTo( *this );
}

void	Game::resetCurrentVal_( void )
{
	memset( CurrentVal_, EMPTY_CELL_VALUE, sizeof(CurrentVal_) );
}

void	Game::resetPendingVal_( void )
{
	memset( PendingVal_, EMPTY_CELL_VALUE, sizeof(PendingVal_) );
}

void	Game::resetCurrentAnimationActions_( void )
{
	memset( CurrentAnimationActions_, 0, sizeof(CurrentAnimationActions_) );
}

void	Game::resetPendingAnimationActions_( void )
{
	memset( PendingAnimationActions_, 0, sizeof(PendingAnimationActions_) );
}

void	Game::setPendingAnimationAction_( int cellIndex, Direction direction, WORD cellCount )
{
	// a really move
	if ( cellCount > 0 )
	{
		PendingAnimationActions_[cellIndex].direction		= direction;
		PendingAnimationActions_[cellIndex].pixelsOffset	= (CANVAS_SPACE+CANVAS_CELL_WIDTH) * cellCount;
		PendingAnimationActions_[cellIndex].pixelsEaten		= ANIMATION_INIT_PIXELS;
	}
}

void	Game::setPendingAnimationAction_( int row, int col, Direction direction, WORD cellCount )
{
	int cellIndex = getCellIndex_( row, col );
	setPendingAnimationAction_( cellIndex, direction, cellCount );
}

void	Game::setCellValue_( UINT* tbl, int row, int col, UINT val )
{
	int cellIndex = getCellIndex_( row, col );

	ASSERT( cellIndex != INVALID_CELL_INDEX );

	tbl[cellIndex] = val;
}

bool	Game::testMove_( Direction direction )
{
	if ( IsOver_ )
	{
		return false;
	}

	// Reset pending context
	resetPendingVal_();
	resetPendingAnimationActions_();

	switch ( direction )
	{
	case LEFT:
		onMoveLeft_();
		break;

	case RIGHT:
		onMoveRight_();
		break;

	case UP:
		onMoveUp_();
		break;

	case DOWN:
		onMoveDown_();
		break;

	default:
		break;
		
	}

	BOOL haveChanges = isContextChanged_();

	return haveChanges ? true : false;
}

void	Game::phaseInPendingVal_( void )
{
	// append to History list, TODO:: set MAX_HISTORY_COUNT
	History_.append( *this );

	updateScore_( );

	// Save Current value for Animation use
	memcpy( PriviousVal_, CurrentVal_, sizeof(CurrentVal_) );
	
	// Phase in pending to release PendingVal_ array for next move
	memcpy( CurrentVal_, PendingVal_, sizeof(PendingVal_) );

	// phase in annimation actions
	phaseInPendingAnimationActions_();

	int emptyCellIndex = lookupEmptyCell_();
	if ( emptyCellIndex != INVALID_CELL_INDEX )
	{
		UINT newCellValue = MINIMUM_CELL_VALUE;
		
		// set to 4:1 ( 2 vs 4 )
		if ( (randEx() % 5 ) == 0 )
		{
			newCellValue = MINIMUM_CELL_VALUE * 2;
		}
		CurrentVal_[ emptyCellIndex ] = newCellValue;
		IsOver_ = detectGameOver_();
	}
}


void	Game::phaseInPendingAnimationActions_( void )
{
	memcpy( CurrentAnimationActions_, PendingAnimationActions_, sizeof(PendingAnimationActions_) );
	if ( AnimationEnabled_ )
	{
		InAnimation_ = TRUE;
		Host::getInstance()->EnableAnimationTimer( TRUE );
	}
}

void	Game::writeDirectionLog_( Direction direction, BOOL haveChanges )
{
    TCHAR msg[128];

    const TCHAR* pMoveDirection = NULL;

	switch ( direction )
	{
	case LEFT:
        pMoveDirection = _T("LEFT");
		break;

	case RIGHT:
        pMoveDirection = _T("RIGHT");
		break;

	case UP:
        pMoveDirection = _T("UP");
		break;

	case DOWN:
        pMoveDirection = _T("DOWN");
		break;
	}

    const TCHAR* pFormat = !haveChanges ? _T("MOVE_%s IGNORED") : _T("MOVE_%s");
    _stprintf_s( msg, pFormat, pMoveDirection );

    Host::getInstance()->WriteLog( msg );
}