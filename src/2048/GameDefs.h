#pragma once

enum CanvasSize
{
	CANVAS_CELL_WIDTH	=	70,
	CANVAS_CELL_HEIGHT	=	CANVAS_CELL_WIDTH,
	CANVAS_SPACE		=	5,
	CANVAS_ROWS			=	4,
	CANVAS_COLUMNS		=	CANVAS_ROWS,
	CANVAS_CELL_COUNT	=	CANVAS_ROWS * CANVAS_COLUMNS,
	CANVAS_WIDTH		=	(CANVAS_CELL_WIDTH + CANVAS_SPACE)
							* CANVAS_ROWS + CANVAS_SPACE,
	CANVAS_HEIGHT		=	(CANVAS_CELL_HEIGHT + CANVAS_SPACE)
							* CANVAS_COLUMNS + CANVAS_SPACE,
};

#define	COUNT_OF(arr) (sizeof(arr)/sizeof(*arr))


enum Direction
{
	NONE,
	LEFT,
	RIGHT,
	UP,
	DOWN,
};

#define	INITIAL_CELLS_COUNT		(2)
#define INVALID_CELL_INDEX		((int)(~0))
#define	INVALID_CELL_VALUE		((int)(~0))
#define EMPTY_CELL_VALUE		(0)
#define MINIMUM_CELL_VALUE		(2)
#define HISTORY_LIMIT_COUNT		(3)
#define DEFAULT_AUTO_STEP_INTERVAL	(500)

#define ANIMATION_INIT_PIXELS	(16)
#define ANIMATION_TIMER_ID		(1)
#define ANIMATION_TIMER_ELAPSE	(60)
#define ANIMATION_ENABLED		(TRUE)


const POINT ROUND_CELL_POINT = { 5, 5 };

enum GameError
{
	SUCCESS,
	KEY_IGNORED,
	GAME_ISOVER,
	INDEX_OUTOF_RANGE,
};