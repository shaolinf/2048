#pragma once
#include "2048.h"
#include "2048Dlg.h"

struct lua_State;
class Interpreter
{
private:
	lua_State* L_;

	int SetLuaPath( lua_State* L, const char* path );

	static int logToHost( lua_State* L );
	static int setStepInterval( lua_State* L );

    static int moveLeft( lua_State* L );
    static int moveRight( lua_State* L );
    static int moveUp( lua_State* L );
    static int moveDown( lua_State* L );

	static int testMoveLeft( lua_State* L );
	static int testMoveRight( lua_State* L );
    static int testMoveUp( lua_State* L );
    static int testMoveDown( lua_State* L );

	static int isGameOver( lua_State* L );

	static int getCellTable( lua_State* L );
	static int getCellValue( lua_State* L );
	static int getRows( lua_State* L );
	static int getColumns( lua_State* L );

	static int getEmptyCellCount( lua_State* L );

	static int sleep( lua_State* L );

public:

	Interpreter(void);
	~Interpreter(void);

	bool RunLuaFile(const TCHAR* fileName, CString& strMsg);
};
