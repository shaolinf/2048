#pragma once

struct Context;

class Profile
{
public:
    static Profile*     instance_;
    static Profile*     getInstance();

    void loadLastContextTo( Context& context );
    void saveContext( const Context& context );
    
	UINT getBestScore( void );
	void updateBestScore( UINT bestScore );

private:
	Profile(void);
    ~Profile(void);

	void loadBestScore_( UINT& bestScore );
    void saveBestScore_( UINT bestScore );

	UINT	BestScore_;
};
