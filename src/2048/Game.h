#pragma once

#include "Context.h"
#include "History.h"
#include "Animation.h"

class Game
    : public Context
{
public:

	static  Game* getInstance();

	static void	draw( CDC* pDC );

	static bool	isOver();

	static void	initialize( void );

	static GameError getCellValue(int index, UINT& value);

	static UINT	getHistoryCount( void );

	static UINT	getScore();

	static UINT getBestScore();

	static GameError	onKey( Direction direction );

	static void	restart();

	static void	reverseOneStep( void );

	static bool testMove( Direction direction );

private:
	Game(void);
	~Game(void);

	class Locker
	{
	public:
		Locker();
		~Locker();
	};

	void	draw_( CDC* pDC );

	bool	isOver_() const;

	void	initialize_( void );

	UINT	getHistoryCount_( void ) const;

	UINT	getScore_() const;

	GameError	onKey_( Direction direction );

	void	restart_();

	void	reverseOneStep_( void );

	///////////////////////////////////

	bool	detectGameOver_();

	void	drawBackground_( CDC* pDC );
	void	drawCells_( CDC* pDC );

	UINT	getCellValue_( int row, int col ) const;
	UINT	getCellValue_( int index ) const;
	int		getCellIndex_( int row, int col ) const;
	int		getRows_( void ) const;
	int		getColumns_( void ) const;

	BOOL	isContextChanged_( void );

	int		lookupEmptyCell_( void ) const;

	void	onMoveLeft_( void );
	void	onMoveRight_( void );
	void	onMoveUp_( void );
	void	onMoveDown_( void );

	void	updateScore_( void );

	void	resetCurrentVal_( void );
	void	resetPendingVal_( void );
	void	resetCurrentAnimationActions_( void );
	void	resetPendingAnimationActions_( void );

	void	setPendingAnimationAction_( int cellIndex, Direction direction, WORD cellCount );
	void	setPendingAnimationAction_( int row, int col, Direction direction, WORD cellCount );

	void	setCellValue_( UINT* tbl, int row, int col, UINT val );

	bool	testMove_( Direction direction );

	void	phaseInPendingVal_( void );
	void	phaseInPendingAnimationActions_( void );

	void	writeDirectionLog_( Direction direction, BOOL haveChanges );

private:

	static Game* instance_;

	static CCriticalSection	cs_;

	History	History_;

	// for annimation
	UINT	PriviousVal_[ CANVAS_CELL_COUNT ];

	// for calculate the next result
	UINT	PendingVal_[ CANVAS_CELL_COUNT ];


	// store the changes for each cell for annimation moving
	AnimationAction		CurrentAnimationActions_[ CANVAS_CELL_COUNT ];
	AnimationAction		PendingAnimationActions_[ CANVAS_CELL_COUNT ];
	BOOL				InAnimation_;

	// how many new scroe is added in current moving
	UINT				NewScore_;


	CFont				TextFont_;

	BOOL				AnimationEnabled_;
};
