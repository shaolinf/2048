#include "StdAfx.h"

#include <string>

#include "Interpreter.h"

#include <lua.hpp>
#include <lauxlib.h>
#include <lualib.h>

#include "Host.h"

UINT gAutoStepInterval = DEFAULT_AUTO_STEP_INTERVAL;

Interpreter::Interpreter( void )
	: L_(NULL)
{
	L_ = lua_open();

	// load various Lua libraries
	luaL_openlibs( L_ );

	// TODO:: do we need this?
	// reference standard library
	SetLuaPath( L_, "C:\\Program Files (x86)\\Lua\\5.1\\lua\\?.lua" );

	lua_register( L_, "logToHost", logToHost );
	lua_register( L_, "setStepInterval", setStepInterval );

    lua_register( L_, "moveLeft", moveLeft );
    lua_register( L_, "moveRight", moveRight );
    lua_register( L_, "moveUp", moveUp );
    lua_register( L_, "moveDown", moveDown );

	lua_register( L_, "testMoveLeft", testMoveLeft );
    lua_register( L_, "testMoveRight", testMoveRight );
    lua_register( L_, "testMoveUp", testMoveUp );
    lua_register( L_, "testMoveDown", testMoveDown );

	lua_register( L_, "isGameOver", isGameOver );

	lua_register( L_, "getCellTable", getCellTable );	
	lua_register( L_, "getCellValue", getCellValue );
	lua_register( L_, "getRows", getRows );
	lua_register( L_, "getColumns", getColumns );

	lua_register( L_, "getEmptyCellCount", getEmptyCellCount );
	lua_register( L_, "sleep", sleep );
}

Interpreter::~Interpreter(void)
{
	lua_close( L_ );
}

bool Interpreter::RunLuaFile(const TCHAR * fileName, CString& strMsg)
{
	Host::getInstance()->WriteLog( fileName );

	char mbsFileName[MAX_PATH];
	wcstombs( mbsFileName, fileName, sizeof(mbsFileName) );

	int error = luaL_dofile( L_, mbsFileName );

	if ( error )
	{
		TCHAR wcsErrorMsg[MAX_LOG_LENGTH];
		mbstowcs( wcsErrorMsg, lua_tostring( L_, -1 ), sizeof(wcsErrorMsg) );
		strMsg.Format(_T("%s"), wcsErrorMsg);

		return false;
	}
	
	return true;
}

int Interpreter::SetLuaPath(lua_State * L, const char * path)
{
	lua_getglobal( L, "package" );
	lua_getfield( L, -1, "path" );					// get field "path" from table at top of stack (-1)
	std::string cur_path = lua_tostring( L, -1 );	// grab path string from top of stack
	cur_path.append( ";" );							// do your path magic here
	cur_path.append( path );
	lua_pop( L, 1 );								// get rid of the string on the stack we just pushed on line 5
	lua_pushstring( L, cur_path.c_str() );			// push the new one
	lua_setfield( L, -2, "path" );					// set the field "path" in table at -2 with value at top of stack
	lua_pop( L, 1 );								// get rid of package table from top of stack

	return 0;										// all done!
}

int Interpreter::logToHost(lua_State * L)
{
	TCHAR buff[MAX_LOG_LENGTH];

	mbstowcs( buff, lua_tostring(L, -1), sizeof(buff) );

	buff[MAX_LOG_LENGTH - 1] = L'\0';

	Host::getInstance()->WriteLog( buff );

	return 0;
}

int Interpreter::setStepInterval( lua_State* L )
{
	int interval = lua_tointeger( L, -1 );

	// limit between 50 and 3000, in ms
	if ( interval <= 50 )
	{
		interval = 50;
	}

	if ( interval > 3000 )
	{
		interval = 3000;
	}

	gAutoStepInterval = interval;

	return 0;
}

int Interpreter::moveLeft( lua_State* L )
{
	GameError code = Game::onKey( LEFT );

    if ( SUCCESS == code )
    {
        Host::getInstance()->RefreshGUI();
        Sleep(gAutoStepInterval);
    }

	lua_pushinteger(L, code);

    return 1;
}

int Interpreter::moveRight( lua_State* L )
{
	GameError code = Game::onKey( RIGHT );

    if ( SUCCESS == code )
    {
        Host::getInstance()->RefreshGUI();
		Sleep(gAutoStepInterval);
    }

    lua_pushinteger(L, code);

    return 1;
}

int Interpreter::moveUp( lua_State* L )
{
	GameError code = Game::onKey( UP );

    if ( SUCCESS == code )
    {
        Host::getInstance()->RefreshGUI();
        Sleep(gAutoStepInterval);
    }

    lua_pushinteger(L, code);

    return 1;
}

int Interpreter::moveDown( lua_State* L )
{
	GameError code = Game::onKey( DOWN );

    if ( SUCCESS == code )
    {
        Host::getInstance()->RefreshGUI();
        Sleep(gAutoStepInterval);
    }

    lua_pushinteger(L, code);

    return 1;
}

int Interpreter::testMoveLeft( lua_State* L )
{
	bool canMove = Game::testMove( LEFT );

	lua_pushboolean(L, canMove);

    return 1;
}

int Interpreter::testMoveRight( lua_State* L )
{
	bool canMove = Game::testMove( RIGHT );

	lua_pushboolean(L, canMove);

    return 1;
}

int Interpreter::testMoveUp( lua_State* L )
{
	bool canMove = Game::testMove( UP );

	lua_pushboolean(L, canMove);

    return 1;
}

int Interpreter::testMoveDown( lua_State* L )
{
	bool canMove = Game::testMove( DOWN );

	lua_pushboolean(L, canMove);

    return 1;
}

int Interpreter::isGameOver( lua_State* L )
{
	bool isOver = Game::isOver();

	lua_pushboolean(L, isOver);

    return 1;
}

int Interpreter::getCellTable( lua_State* L )
{
	lua_newtable(L);

	for ( int index = 0; index < CANVAS_CELL_COUNT; ++ index )
	{
		lua_pushinteger(L, index);
		
		UINT cellValue = 0;
		
		GameError code = Game::getCellValue( index, cellValue );

		ASSERT( code == SUCCESS );

		lua_pushinteger(L, cellValue);
		lua_settable(L, -3);
	}

	return 1;
}

int Interpreter::getCellValue( lua_State* L )
{
	int cellIndex = lua_tointeger( L, -1 );

	UINT cellValue = 0;
	GameError code = Game::getCellValue( cellIndex, cellValue );

	if ( code == SUCCESS )
	{
		lua_pushinteger(L, cellValue);
	}
	else
	{
		lua_pushnil(L);
	}

	return 1;
}

int Interpreter::getRows( lua_State* L )
{
	lua_pushinteger(L, CANVAS_ROWS);

	return 1;
}

int Interpreter::getColumns( lua_State* L )
{
	lua_pushinteger(L, CANVAS_COLUMNS);

	return 1;
}

int Interpreter::getEmptyCellCount( lua_State* L )
{
	int emptyCellCount = 0;

	for ( int index = 0; index < CANVAS_CELL_COUNT; ++ index )
	{		
		UINT cellValue = 0;
		GameError code = Game::getCellValue( index, cellValue );
		ASSERT( code == SUCCESS );

		if ( cellValue == EMPTY_CELL_VALUE )
		{
			emptyCellCount ++;
		}
	}

	lua_pushinteger(L, emptyCellCount);

	return 1;
}

int Interpreter::sleep( lua_State* L )
{
	int ms = lua_tointeger( L, -1 );
	Sleep(ms);

	return 0;
}