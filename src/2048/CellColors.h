#pragma once

#define BKGND_COLOR			RGB(160, 160, 160)
#define DEFAULT_CELL_COLOR	RGB(255, 106, 0)
#define EMPTY_CELL_COLOR	RGB(200, 200, 200)

class CellColors
{
public:

	static CellColors* getInstance();

	COLORREF	get( UINT val );

private:
	CellColors(void);
	~CellColors(void);

	static CellColors*		instance_;

	map<UINT, COLORREF>		colorMap_;

};
