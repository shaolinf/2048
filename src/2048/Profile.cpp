#include "StdAfx.h"
#include "Profile.h"
#include "Context.h"

Profile* Profile::instance_ = NULL;

Profile::Profile(void)
	: BestScore_(0)
{
	loadBestScore_( BestScore_ );
}

Profile::~Profile(void)
{
}


void Profile::loadLastContextTo(Context& context)
{
    bool isOver = AfxGetApp()->GetProfileInt(_T("context"), _T("GameIsOver"), 1) ? true : false;

    if ( !isOver )
    {
        context.IsOver_     = isOver;
        UINT    bytesRead   = 0;
        LPBYTE  pData       = NULL;

        AfxGetApp()->GetProfileBinary(_T("context"), _T("CurrentVal"), &pData, &bytesRead);
        ASSERT( bytesRead == sizeof(context.CurrentVal_) && pData != NULL );
        memcpy( context.CurrentVal_, pData, bytesRead );
        delete[] pData;

        context.Score_ = AfxGetApp()->GetProfileInt(_T("context"), _T("Score"), 0);
    }
}

void Profile::saveContext(const Context & context)
{
	AfxGetApp()->WriteProfileInt(_T("context"), _T("GameIsOver"), context.IsOver_);

	// only allow save when Game is not over
	if ( !context.IsOver_ )
	{
		AfxGetApp()->WriteProfileBinary(_T("context"), _T("CurrentVal"), (LPBYTE)context.CurrentVal_, sizeof(context.CurrentVal_));
		AfxGetApp()->WriteProfileInt(_T("context"), _T("Score"), context.Score_);
	}
}

UINT Profile::getBestScore( void )
{
	return BestScore_;
}

void Profile::updateBestScore( UINT bestScore )
{
	if ( bestScore > BestScore_ )
	{
		BestScore_ = bestScore;
		saveBestScore_( BestScore_ );
	}
}


void Profile::loadBestScore_(UINT & bestScore)
{
    bestScore = AfxGetApp()->GetProfileInt(_T("default"), _T("HighScore"), 0);
}

void Profile::saveBestScore_(UINT bestScore)
{
    AfxGetApp()->WriteProfileInt(_T("default"), _T("HighScore"), bestScore);
}

Profile * Profile::getInstance()
{
    if ( instance_ == NULL )
    {
        instance_ = new Profile();
    }
    return instance_;
}