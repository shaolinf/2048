DESCRIPTION:

  Use UP, DOWN, LEFT, RIGHT keys to move blocks, if two blocks have the 
  same number and they hit together, the two blocks will be merged together.

CONTACT: shaolinfu@gmail.com

VERSION HISTORY:

1.0.0.1 - DATE 04/10/2014
  First release. Basic features, use [UP, DOWN, LEFT, RIGHT] to move blocks.

1.0.0.2	- DATE 04/12/2014
  Fixed a crash bug when reach to 1024.

1.0.0.3 - DATE 04/15/2014
  Save the game when closing, allow UNDO (3) steps.

1.0.1.0 - DATE 04/27/2014
  Add lua script support, see readme.lua for details.