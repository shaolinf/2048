#include "StdAfx.h"
#include "Canvas.h"
#include "Game.h"

Canvas::Canvas(void)
:width_(0),
height_(0)
{
}

Canvas::~Canvas(void)
{
}

void	Canvas::initialize( int width, int height )
{
	width_ = width;
	height_ = height;

	// initiate the CDC and bitmap for drawing
	CDC* pDC = AfxGetApp()->m_pMainWnd->GetDC();

	memDC_.CreateCompatibleDC( pDC );

	memBmp_.CreateCompatibleBitmap( pDC, width, height );

	memDC_.SelectObject( &memBmp_ );

	AfxGetApp()->m_pMainWnd->ReleaseDC( pDC );

	infoAreaRect_.left   = CANVAS_WIDTH;
	infoAreaRect_.right  = width;
	infoAreaRect_.top	 = 0;
	infoAreaRect_.bottom = height;

	int infoLineWidth = CANVAS_CELL_WIDTH / 3 * 2;

	// best score display rectangle
	bestScoreDisplayRect_.left = CANVAS_WIDTH;
	bestScoreDisplayRect_.top = 0;
	bestScoreDisplayRect_.right = width;
	bestScoreDisplayRect_.bottom = bestScoreDisplayRect_.top + infoLineWidth;

	scoreDisplayRect_ = bestScoreDisplayRect_;
	scoreDisplayRect_.OffsetRect( 0, infoLineWidth );

	gameOverDisplayRect_ = scoreDisplayRect_;
	gameOverDisplayRect_.OffsetRect( 0, infoLineWidth );

	scoreFont_.CreateFont(
		32,                        // nHeight
		0,                         // nWidth
		0,                         // nEscapement
		0,                         // nOrientation
		FW_NORMAL,                 // nWeight
		FALSE,                     // bItalic
		FALSE,                     // bUnderline
		0,                         // cStrikeOut
		ANSI_CHARSET,              // nCharSet
		OUT_DEFAULT_PRECIS,        // nOutPrecision
		CLIP_DEFAULT_PRECIS,       // nClipPrecision
		DEFAULT_QUALITY,           // nQuality
		DEFAULT_PITCH | FF_SWISS,  // nPitchAndFamily
		_T("Arial") );                 // lpszFacename 
}

void	Canvas::draw(CDC* pDC, int x, int y)
{
	drawMain_();
	drawInfoBkgnd_();
	drawInfo_();

	pDC->BitBlt( x, y, width_, height_, &memDC_, 0, 0, SRCCOPY );
}

void	Canvas::drawMain_()
{
	Game::draw( &memDC_ );
}

void	Canvas::drawInfoBkgnd_()
{
	CBrush brush(::GetSysColor(COLOR_3DFACE));

	memDC_.FillRect( &infoAreaRect_, &brush );
}

void	Canvas::drawInfo_()
{
	// draw score
	UINT score = Game::getScore();
	UINT bestScore = Game::getBestScore();

	TCHAR bestScoreText[64];
	TCHAR scoreText[64];

	_stprintf_s( bestScoreText, _T("BEST: %u"), bestScore );
	_stprintf_s( scoreText, _T("SCORE: %d"), score );

	int oldMode = memDC_.SetBkMode( TRANSPARENT );

	CFont* pOldFont = memDC_.SelectObject( &scoreFont_ );

	memDC_.DrawText( bestScoreText, _tcslen(bestScoreText), &bestScoreDisplayRect_, DT_SINGLELINE|DT_CENTER|DT_VCENTER );
	memDC_.DrawText( scoreText, _tcslen(scoreText), &scoreDisplayRect_, DT_SINGLELINE|DT_CENTER|DT_VCENTER );

	bool gameIsOver = Game::isOver();

	if ( gameIsOver )
	{
		COLORREF oldColor = memDC_.SetTextColor(RGB(255,0,0));
		memDC_.DrawText( _T("GAME OVER!"), -1, &gameOverDisplayRect_, DT_SINGLELINE|DT_CENTER|DT_VCENTER );
		memDC_.SetTextColor(oldColor);
	}

	memDC_.SetBkMode( oldMode );
	memDC_.SelectObject( pOldFont );
}