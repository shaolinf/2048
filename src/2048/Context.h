#pragma once
#include "GameDefs.h"

class Profile;

struct Context
{
    friend class Profile;

protected:
    UINT	CurrentVal_[ CANVAS_CELL_COUNT ];
    bool    IsOver_;
    UINT    Score_;
};