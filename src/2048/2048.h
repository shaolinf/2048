
// 2048.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols


// CMy2048App:
// See 2048.cpp for the implementation of this class
//

class CMy2048App : public CWinAppEx
{
public:
	CMy2048App();

// Overrides
	public:
	virtual BOOL InitInstance();

// Implementation

	DECLARE_MESSAGE_MAP()
};

extern CMy2048App theApp;