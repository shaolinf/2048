#pragma once
#include "Context.h"

class History
{
public:
	History(UINT limit);
	~History(void);

	void	append( const Context& context );
	
	void	clear(void);

	UINT	getCount( void ) const;

	void	reverseTo( Context& context );
	

private:

	void	notifyChange_();

	UINT			Limit_;

	deque<Context>	HistoryContext_;
};
